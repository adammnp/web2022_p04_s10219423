﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10219423.Models
{
    public class Staff
    {   
        [Display(Name = "ID")]
       
        public int StaffId { get; set; }
        
        public string Name { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }

        public string Nationality { get; set; }
        [Display(Name = "Email Address")]
        [RegularExpression(@"(\w[-.\w]*\w@\w[-.\w]*\w.\w{2,3})")]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string Email { get; set; }

        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00, 10000.00)]
        public decimal Salary { get; set; }

        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }

        [Display(Name = "Branch")]
        public int? BranchNo { get; set; }
        public char Gender { get; set; }

    }

}
